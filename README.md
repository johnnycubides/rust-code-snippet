# Notas

## Cargo

* `cargo new <name_app> --bin`: crea un proyecto que será una aplicación


## Instalar RUST

```bash
curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh
```

## Actualizar

```bash
rustup update
```


## Glosario

* crates


## Referencias

[Iniciarse con rust](https://www.rust-lang.org/es/learn/get-started)

[Libro de rust](https://doc.rust-lang.org/book/)

[ejemplos de rust](https://doc.rust-lang.org/rust-by-example/index.html)

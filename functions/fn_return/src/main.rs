// #[derive(Debug)]

fn multiplicacion1(a:f32, b:f32) -> f32 {
   return a*b; // returno desde una operación
}

fn multiplicacion2(a:f32, b:f32) -> f32  {
    let c:f32 = a*b;
    return c; // retorno con una variable, en este caso inmutable
}

fn multiplicacion3(a:f32, b:f32) -> f32  {
    a*b // returno implicito
}

fn main() {
    println!("Hello, world!");
    println!("{:?}", multiplicacion1(2.0, 3.0));
    println!("{:?}", multiplicacion2(2.0, 3.0));
    println!("{:?}", multiplicacion3(2.0, 3.0));
}

// La estructura declarada
struct Persona {
    id: i32,
    edad: i32,
}

// Una función que devuelve una estructura para referenciar
fn crear_persona(id:i32, edad:i32) -> Persona {
    Persona {id: id, edad: edad}
}

// Metodos asociados a una estructura en particular
impl Persona {

    // Metodo asociado a la estructura
    fn mostrar_detalles(self: &Self) {
        println!("Identificación: {}", self.id);
        println!("Edad: {}", self.edad);
    }

    // Un metodo asociado a la estructura capáz de creas un objeto
    // a partir de esa estructura Persona::crear_persona(id: i32, edad: i32)
    fn crear_persona(id:i32, edad:i32) -> Persona {
        Persona {id: id, edad: edad}
    }

    // Un método que puede cambiar un miembro de la estructura, la estructura
    // debe ser instanciada como mutable
    fn cambiar_edad(&mut self, nueva_edad: i32) {
        self.edad = nueva_edad;
        println!("Edad cambiada a: {}", self.edad);
    }
}

fn main() {
    // Una estructura instanciada de manera manual
    let persona1: Persona = Persona {id:13617902, edad: 36};
    println!("{}, {}", persona1.id, persona1.edad);

    // Una estructura instanciada por la función que retorna este tipo
    // de objeto
    let persona2: Persona = crear_persona(1010203523, 29);
    println!("{}, {}", persona2.id, persona2.edad);

    // Una estructura creada a partir del método asociado
    let persona3 = Persona::crear_persona(28208641, 52);
    persona3.mostrar_detalles();

    // Una estructura mutable instanciada por el metodo asociado
    let persona4 = &mut Persona::crear_persona(1, 11);
    persona4.mostrar_detalles();
    // Cambio del valor de unos de los miembros de la estructura
    // tener presnte que esto es posible si se ha declarado la estructura
    // como mutable
    persona4.cambiar_edad(12);
}
